namespace MultichoiceCollection.Models.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invperiod : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "InvoicePeriod", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transactions", "InvoicePeriod");
        }
    }
}
