﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using MultichoiceCollection.Common.Entities;
using MultichoiceCollection.Common.Entities.Enum;
using MultichoiceCollection.Models.Repositories.Context;
using MultichoiceCollection.Presentation.Areas.Admin.Models;
using MultichoiceCollection.Presentation.Attributes;
using MultichoiceCollection.Presentation.Models;
using MultichoiceCollection.Presentation.Services;
using MultichoiceCollection.Services;
using MultichoiceCollection.Services.Implementations;

namespace MultichoiceCollection.Presentation.Controllers
{
     [CustomAuthorize]
     public class HomeController : BaseController
     {
          readonly ApiRequestService _request;
          private readonly AppDbContext _appDbContext;
          public HomeController()
          {
               _request = new ApiRequestService();
               _appDbContext = new AppDbContext();

          }
          public ActionResult Index()
          {
               AddAuditInfo(new AuditTrail
               {
                    Title = "View Agent Dashboard",
                    Detail = $"Payload: ",
                    RefUrl = Url.Action("Index")
               });
               var user = _appDbContext.Users.Single(u => u.UserName.Equals(User.Identity.Name));
               if (user.ShouldChangePassword)
               {
                    ShowMessage("Please update your password to continue", AlertType.Danger);
                    return RedirectToAction("ChangePassword", "Account");
               }
               var accountNumber = user.AccountNumber;
               var today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(-1);
               var tomorrow = today.AddDays(1);
               // ViewBag.TotalSales = _appDbContext.Transactions.Sum(tr => tr.amount).ToString("N");

               //Getting the Current Transaction for the logged in User
               var transactions = _appDbContext.Transactions.Where(t => t.success && t.accountNumber == accountNumber).ToList();

               ViewBag.TotalSales = transactions.Sum(tr => tr.amount);
               if (transactions.Count() > 0)
               {

                    //ViewBag.TotalSales = transactions.Sum(tr => tr.amount);
                    if (transactions.Count(e => e.CreatedDate.Date == DateTime.Now.Date) > 0)
                    {
                         ViewBag.TodaySales = transactions.Where(t => t.CreatedDate.Date == DateTime.Now.Date).Sum(t => t.amount);
                    }
                    else
                    {
                         ViewBag.TodaySales = "0.00";
                    }

                    //ViewBag.TodaySales = _appDbContext.Transactions.Where(t => t.accountNumber == accountNumber && (t.CreatedDate >= today && t.CreatedDate < tomorrow)).Sum(tr => tr.amount);

                    //ViewBag.TodaySales = _appDbContext.Transactions.Where(t => t.accountNumber == accountNumber && t.CreatedDate.Date == DateTime.Now.Date).Sum(tr => tr.amount);

                    ViewBag.PrintedReceipts = _appDbContext.Transactions.Where(t => t.accountNumber == accountNumber).Count(tr => tr.PrintedReceipt);
                    ViewBag.InceptionDate = _appDbContext.Transactions.OrderBy(e => e.CreatedDate).FirstOrDefault().CreatedDate;


                    //ViewBag.LatestTransactions = _appDbContext.Transactions.Where(t => t.accountNumber == accountNumber).OrderByDescending(tr => tr.id).Take(10).ToList();
               }
               else
               {
                    //ViewBag.TotalSales = "0.00";
                    ViewBag.TodaySales = "0.00";
               }

               ViewBag.CurrentDashboard = "current";
               return View();
          }
          public ActionResult BouquetTypes()
          {
               AddAuditInfo(new AuditTrail
               {
                    Title = "View Bouquet Types",
                    Detail = "Payload: ",
                    RefUrl = Url.Action("BouquetTypes")
               });
               ViewBag.Types = "current";
               var response = new List<BouquetTypeModel>();
               try
               {

                    response = _request.MakeRequest<List<BouquetTypeModel>>(ApiConstantService.TYPES);
                    return View(response);
               }
               catch (Exception exception)
               {
                    Logger.WriteErrorLog(exception);
                    ShowMessage(exception.Message, AlertType.Danger);
                    return View(response);
               }

          }
          public ActionResult Bouquets(string type)
          {
               AddAuditInfo(new AuditTrail
               {
                    Title = "View Bouquets",
                    Detail = $"Payload: {type}",
                    RefUrl = Url.Action("BouquetTypes", new { type })
               });
               ViewBag.Bouquets = "current";
               if (string.IsNullOrEmpty(type))
               {
                    ShowMessage("Type of bouquet must be selected", AlertType.Danger);
                    return RedirectToAction("Index");
               }
               if (!type.Equals("gotv") && !type.Equals("dstv"))
               {
                    ShowMessage("Bouquet type can only be gotv or dstv", AlertType.Danger);
                    return RedirectToAction("Index");
               }
               var response = new List<BouquetModel>();
               try
               {
                    var url = ApiConstantService.BASE_URL + type + "/bouquet";
                    response = _request.MakeRequest<List<BouquetModel>>(url);
                    return View(response);
               }
               catch (Exception exception)
               {
                    Logger.WriteErrorLog(exception);
                    ShowMessage(exception.Message, AlertType.Danger);
                    return View(response);
               }

          }


          public ActionResult Accounts(string type, string cardNumber)
          {
               AddAuditInfo(new AuditTrail
               {
                    Title = "View Accounts",
                    Detail = $"Type: {type}, Card Number: {cardNumber}",
                    RefUrl = Url.Action("Accounts", new { type, cardNumber })
               });
               ViewBag.Accounts = "current";
               if (string.IsNullOrEmpty(type))
               {
                    ShowMessage("Type of bouquet must be selected", AlertType.Danger);
                    return RedirectToAction("Index");
               }
               if (!type.Equals("gotv") && !type.Equals("dstv"))
               {
                    ShowMessage("Bouquet type can only be gotv or dstv", AlertType.Danger);
                    return RedirectToAction("Index");
               }
               if (string.IsNullOrEmpty(cardNumber))
               {
                    ShowMessage("Smart card number is required.", AlertType.Danger);
                    return RedirectToAction("Index");
               }
               var response = new CustomerAccountModel();
               try
               {
                    var url = ApiConstantService.BASE_URL + type + "/accounts/" + cardNumber;
                    response = _request.MakeRequest<CustomerAccountModel>(url);
                    return View(response);
               }
               catch (Exception exception)
               {
                    Logger.WriteErrorLog(exception);
                    ShowMessage(exception.Message, AlertType.Danger);
                    return View(response);
               }
          }

          public ActionResult BouquetDetails(string type, string cardNumber)
          {
               AddAuditInfo(new AuditTrail
               {
                    Title = "View Bouquet Types",
                    Detail = $"Type: {type}, Card Number: {cardNumber} ",
                    RefUrl = Url.Action("BouquetTypes")
               });
               ViewBag.BouquetDetails = "current";
               if (string.IsNullOrEmpty(type))
               {
                    ShowMessage("Type of bouquet must be selected", AlertType.Danger);
                    return RedirectToAction("Index");
               }
               if (!type.Equals("gotv") && !type.Equals("dstv"))
               {
                    ShowMessage("Bouquet type can only be gotv or dstv", AlertType.Danger);
                    return RedirectToAction("Index");
               }
               if (string.IsNullOrEmpty(cardNumber))
               {
                    ShowMessage("Smart card number is required.", AlertType.Danger);
                    return RedirectToAction("Index");
               }
               var response = new CustomerDetailsModel();
               try
               {
                    var url = ApiConstantService.BASE_URL + type + "/details/" + cardNumber;
                    response = _request.MakeRequest<CustomerDetailsModel>(url);
                    return View(response);
               }
               catch (Exception exception)
               {
                    Logger.WriteErrorLog(exception);
                    ShowMessage(exception.Message, AlertType.Danger);
                    return View(response);
               }
          }

          public PartialViewResult Transactions(int page = 1, DateTime? startDate = null, DateTime? endDate = null, string smartCardNumber = null)
          {
               var user = _appDbContext.Users.Single(e => e.UserName.Equals(User.Identity.Name));

               if (page < 1) page = 1;

               ViewBag.StartDate = startDate;
               ViewBag.EndDate = endDate;
               ViewBag.SmartCardNumber = smartCardNumber;


               ViewBag.Transactions = "current";
               using (var context = new AppDbContext())
               {
                    var query = context.Transactions.Where(t => t.success && t.accountNumber == user.AccountNumber);
                    if (endDate.HasValue)
                    {
                         query = query.Where(q => q.CreatedDate >= startDate && q.CreatedDate <= endDate);
                    }
                    if (!string.IsNullOrEmpty(smartCardNumber))
                    {
                         query = query.Where(t => t.customerNumber == smartCardNumber);
                    }

                    var total = query.Count();

                    var offset = (page - 1) * 50;
                    var transactions = query.OrderByDescending(t => t.CreatedDate).Skip(offset).Take(10);

                    if (!transactions.Any())
                    {
                         ShowMessage("No Records Found", AlertType.Danger);
                    }

                    var vm = new TransactionViewModel
                    {
                         Transactions = transactions.ToList(),
                         PageSize = 10,
                         Page = page,
                         Total = total,
                         SmartCardNumber = smartCardNumber
                    };

                    return PartialView(vm);
               }

          }


          public void Download(DateTime? startDate, DateTime? endDate, string smartCardNumber = null)
          {
               var user = _appDbContext.Users.Single(e => e.UserName.Equals(User.Identity.Name));

               using (var context = new AppDbContext())
               {
                    var query = context.Transactions.Where(t => t.success && t.accountNumber == user.AccountNumber);

                    //I removed this to see if the download will work

                    if (endDate > DateTime.MinValue)
                    {
                         query = query.Where(q => q.CreatedDate <= endDate);
                    }

                    if (startDate > DateTime.MinValue)
                    {
                         query = query.Where(q => q.CreatedDate >= startDate);
                    }

                    if (!smartCardNumber.Trim().Equals(""))
                    {
                         query = query.Where(q => q.customerNumber.Equals(smartCardNumber));
                    }


                    var sb = new StringBuilder();
                    var list = query.ToList();
                    sb.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", "S/No", "Agent Till", "Amount", "Fees ", "Customer Number",
                        "Phone", "Trans Date", "Audit Ref No", "Device No", "Business Type", "Creation Date", Environment.NewLine);
                    var sn = 0;
                    foreach (var item in list)
                    {
                         sb.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", ++sn, item.accountNumber,
                             item.amount,
                             item.transFees, item.customerNumber, item.mobileNumber, item.transactionDate,
                             item.auditReferenceNo, item.deviceNumber, item.businessType,
                             item.CreatedDate.ToShortDateString(),
                             Environment.NewLine);
                    }
                    //Get Current Response  
                    var response = System.Web.HttpContext.Current.Response;
                    response.BufferOutput = true;
                    response.Clear();
                    response.ClearHeaders();
                    response.ContentEncoding = Encoding.Unicode;
                    response.AddHeader("content-disposition", $"attachment;filename={user.AccountNumber}-{DateTime.Now.ToString()}-Report.CSV");
                    response.ContentType = "text/plain";
                    response.Write(sb.ToString());
                    response.End();
               }

          }
     }
}