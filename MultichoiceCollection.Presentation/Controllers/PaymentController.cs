﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using MultichoiceCollection.Common.Entities;
using MultichoiceCollection.Common.Entities.Enum;
using MultichoiceCollection.Models.Repositories.Context;
using MultichoiceCollection.Models.Repositories.Implementations;
using MultichoiceCollection.Presentation.Attributes;
using MultichoiceCollection.Presentation.Models;
using MultichoiceCollection.Presentation.Services;
using MultichoiceCollection.Services;
using MultichoiceCollection.Services.Implementations;

namespace MultichoiceCollection.Presentation.Controllers
{
     [Authorize]
     public class PaymentController : BaseController
     {
          private readonly ApiRequestService _request;
          private AppDbContext _appDbContext;
          public PaymentController()
          {
               _request = new ApiRequestService();
               this._appDbContext = new AppDbContext();
          }
          // GET: Payment
          public ActionResult Index(string id)
          {
               AddAuditInfo(new AuditTrail
               {
                    Title = "View Payment Page",
                    Detail = id,
                    RefUrl = Url.Action("Index", new { id })
               });
               var username = User.Identity.Name;
               if (string.IsNullOrEmpty(id))
               {
                    this.ShowMessage("Payment type is required.", AlertType.Danger);
                    return RedirectToAction("Index", "Home");
               }
               ViewBag.CurrentPayment = "current";
               if (id == "customerNumber")
               {
                    ViewBag.PaymentType = "makePaymentCustomerNumberModal";
                    ViewBag.PayEl = CustomerAccountPayEl;
               }
               else if (id == "smartCardNumber")
               {
                    ViewBag.PaymentType = "makePaymentSmartCardNumberModal";
                    ViewBag.PayEl = SmartCardPayEl;
               }
               return View();
          }

          public ActionResult MakePaymentCustomerNumber()
          {
               return RedirectToAction("Index", "Home");
          }

          [HttpPost]
          public ActionResult PreviewMakePaymentCustomerNumber(MakePaymentCustomerNumberModel model)
          {
               Session[CustomerAccountPayEl] = model;
               return View(model);
          }

          [HttpPost]
          public ActionResult MakePaymentCustomerNumber(MakePaymentCustomerNumberModel model)
          {
               Session.Remove(CustomerAccountPayEl);
               string type = model.Type;
               string productKey = model.ProductKey;
               string customernumber = model.Customernumber;
               string basketId = model.BasketId;

               decimal amount = model.Amount;
               int invoicePeriod = model.InvoicePeriod;
               string emailAddress = model.EmailAddress;
               string mobileNumber = model.MobileNumber;
               string paymentMode = model.PaymentMode;
               AddAuditInfo(new AuditTrail
               {
                    Title = "Make Payment Customer Number",
                    Detail = $"Type: {type}, Product Key: {productKey}, Customer Number: {customernumber}, Basket Id: {basketId}, Amount: {amount}, " +
                            $"Invoice Period: {invoicePeriod}, Email Address: {emailAddress}, Mobile Number: {mobileNumber}, Payment Mode: {paymentMode} ",
                    RefUrl = Url.Action("MakePaymentCustomerNumber")
               });
               if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(productKey) ||
                          string.IsNullOrEmpty(customernumber) ||
                          string.IsNullOrEmpty(emailAddress) ||
                          string.IsNullOrEmpty(mobileNumber)
                          || string.IsNullOrEmpty(paymentMode))
               {
                    ShowMessage("All the fields are required.", AlertType.Warning);
                    return RedirectToAction("Index", "Home");
               }
               ViewBag.MakePaymentCustomerNumber = "current";
               var response = new MakePaymentCustomerNumberResponseModel();
               try
               {
                    var user = _appDbContext.Users.First(u => u.UserName == User.Identity.Name);
                    var accountNumber = user.AccountNumber;

                    var url = ApiConstantService.BASE_URL + type.ToLower() + "/payment/" + productKey + "/" + customernumber;
                    var headers = new NameValueCollection();
                    headers.Add("Authorization", "bearer " + this.UserInfo.accessToken);
                    var postData = new Dictionary<string, string>();
                    postData.Add("amount", amount + "");
                    postData.Add("invoicePeriod", invoicePeriod + "");
                    postData.Add("emailAddress", emailAddress);
                    postData.Add("accountNumber", accountNumber);
                    postData.Add("mobileNumber", mobileNumber);
                    postData.Add("paymentMode", paymentMode);
                    response = _request.MakeRequest<MakePaymentCustomerNumberResponseModel>(url, postData, HttpVerb.POST, headers);
                    if (response != null)
                    {
                         var transactionEnquiriesUrl = ApiConstantService.BASE_URL + "transactions/" +
                                                       response.transactionNumber;
                         var transactionEnquiries = _request.MakeRequest<TransactionsResponseModel>(transactionEnquiriesUrl);
                         var transaction = _appDbContext.Transactions.Add(new Transaction
                         {
                              accountNumber = transactionEnquiries.accountNumber,
                              transactionDate = transactionEnquiries.transactionDate,
                              apiClientId = transactionEnquiries.apiClientId,
                              customerNumber = customernumber,
                              deviceNumber = transactionEnquiries.deviceNumber,
                              packageCode = transactionEnquiries.packageCode,
                              businessType = transactionEnquiries.businessType,
                              emailAddress = transactionEnquiries.emailAddress,
                              mobileNumber = transactionEnquiries.mobileNumber,
                              amount = transactionEnquiries.amount,
                              transFees = transactionEnquiries.transFees,
                              posted = transactionEnquiries.posted,
                              auditReferenceNo = transactionEnquiries.auditReferenceNo,
                              success = transactionEnquiries.success,
                              url = transactionEnquiries.url,
                              CreatedDate = DateTime.UtcNow,
                              AgentName = user.UserName,
                              InvoicePeriod = invoicePeriod,

                         });
                         if (this._appDbContext.SaveChanges() > 0)
                         {
                              ViewBag.Id = transaction.id;
                         }
                         ShowMessage("Payment Successfull", AlertType.Success);
                    }
                    else ShowMessage("Error in making payment", AlertType.Danger);

                    return View(response);
               }
               catch (Exception exception)
               {
                    Logger.WriteErrorLog(exception);
                    this.ShowMessage(exception.Message, AlertType.Danger);
                    return View(response);
               }
          }

          public ActionResult MakePaymentSmartCardNumber()
          {
               return RedirectToAction("Index", "Home");
          }

          public const string CustomerAccountPayEl = "can";
          public const string SmartCardPayEl = "sc";

          // SMARTCARD NUMBER FLOW
          // 1. Validates the SmartCard Number
          public ActionResult ValidateNumberForPayment(string type, string cardNumber, string payel, string paymentType)
          {
               if (string.IsNullOrEmpty(type))
               {
                    this.ShowMessage("Type of bouquet must be selected", AlertType.Danger);
                    return RedirectToAction("Index", new { id = type });
               }
               if (!type.Equals("gotv") && !type.Equals("dstv"))
               {
                    this.ShowMessage("Bouquet type can only be gotv or dstv", AlertType.Danger);
                    return RedirectToAction("Index", new { id = type });
               }
               if (string.IsNullOrEmpty(cardNumber))
               {
                    this.ShowMessage("Smart card number is required.", AlertType.Danger);
                    return RedirectToAction("Index", new { id = type });
               }
               if (string.IsNullOrEmpty(paymentType))
               {
                    this.ShowMessage("Payment Type is required.", AlertType.Danger);
                    return RedirectToAction("Index", new { id = type });
               }

               var response = new CustomerAccountModel();
               try
               {
                    var url = ApiConstantService.BASE_URL + type + "/accounts/" + cardNumber;
                    response = _request.MakeRequest<CustomerAccountModel>(url);
                    if (response == null)
                    {
                         ShowMessage("Invalid card number", AlertType.Danger);
                         return RedirectToAction("Index", new { id = type });
                    }

                    // This is the new payment flow.
                    /*
                    switch (paymentType)
                    {
                         case "updown":
                              return RedirectToAction("MakePaymentUpgrade", new { type, cardNumber, payel, paymentType });
                         case "boxoffice":
                              return RedirectToAction("MakePaymentBoxOffice", new { type, cardNumber, payel, paymentType });
                         default:
                              return RedirectToAction("MakePaymentRenewal", new { type, cardNumber, payel, paymentType });
                    }
                    */

                    // This is for the previous payment flow
                    // After Validation of SmartCard Number we redirect to 2nd Stage for SmartCard  Number Flow
                    return RedirectToAction("MakePayment", new { type, cardNumber, payel, paymentType });
               }
               catch (Exception exception)
               {
                    Logger.WriteErrorLog(exception);
                    ShowMessage(exception.Message, AlertType.Danger);
                    return RedirectToAction("MakePayment", new { type, cardNumber, payel, paymentType });
                    return RedirectToAction("Index", new { id = payel });
               }
          }

          // SMARTCARD NUMBER FLOW
          // 2. Try to Call the Payment Endpoint for SmartCard Number
          public ActionResult MakePayment(string type, string payel, string paymentType, string cardNumber = "")
          {
               if (string.IsNullOrEmpty(payel))
               {
                    return RedirectToAction("Index", new { id = type });
               }
               var response = new List<BouquetModel>();
               try
               {
                    var url = ApiConstantService.BASE_URL + type + "/bouquet";
                    response = _request.MakeRequest<List<BouquetModel>>(url);

               }
               catch (Exception exception)
               {
                    Logger.WriteErrorLog(exception);
                    ShowMessage(exception.Message, AlertType.Danger);
                    response = new List<BouquetModel>();
                    //return RedirectToAction("Index");
               }

               var model = Session[payel];

               var vm = new MakePaymentViewModel
               {
                    Type = type,
                    Bouquets = response,
                    PayEl = payel,
                    CardNumber = cardNumber,
                    Model = model,

                    // This is used for the paymentType new flow
                    PaymentType = paymentType
               };

               var selectVM = new SelectBouquetViewModel
               {
                    Type = type,
                    Bouquets = response,
                    PayEl = payel,
                    CardNumber = cardNumber,
                    Period = 1
               };

               switch (paymentType)
               {
                    case "updown":
                         return View("SelectBouquetUpgrade", selectVM);
                    case "boxoffice":
                         return View("MakePayment_sc", vm);
                    case "topup":
                         return View("MakePayment_sc", vm);
                    default:
                         return View("SelectBouquetRenewal", selectVM);
               }

               //return View($"MakePayment_{payel}", vm);
          }

          public ActionResult PreviewMakePaymentSmartCardNumber(PreviewMakePaymentSmartCardNumberModel model)
          {
               Session[SmartCardPayEl] = model;
               return View(model);
          }

          [HttpPost]
          public ActionResult MakePaymentSmartCardNumber(
              PreviewMakePaymentSmartCardNumberModel model
              )
          {
               Session.Remove(SmartCardPayEl);
               ViewBag.PaymentSucceded = false;

               var type = model.Type;
               var productKey = model.ProductKey;
               var smartcardnumber = model.Smartcardnumber;
               var amount = model.Amount;
               var invoicePeriod = model.InvoicePeriod;
               var emailAddress = model.EmailAddress;
               var mobileNumber = model.MobileNumber;
               var paymentDescription = model.PaymentDescription;
               var paymentMode = model.PaymentMode;

               AddAuditInfo(new AuditTrail
               {
                    Title = "Make Payment Customer Number",
                    Detail = $"Type: {type}, Product Key: {productKey}, Smart Card Number: {smartcardnumber},  Amount: {amount}, " +
                            $"Invoice Period: {invoicePeriod}, Email Address: {emailAddress}, Mobile Number: {mobileNumber}, Payment Mode: {paymentMode} ",
                    RefUrl = Url.Action("MakePaymentSmartCardNumber")
               });
               if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(productKey) ||
                          string.IsNullOrEmpty(smartcardnumber) ||
                          string.IsNullOrEmpty(emailAddress) ||
                          string.IsNullOrEmpty(mobileNumber)
                          || string.IsNullOrEmpty(paymentDescription) || string.IsNullOrEmpty(paymentMode))
               {
                    ShowMessage("All the fields are required.", AlertType.Warning);
                    return RedirectToAction("Index", "Home");
               }

               var user = _appDbContext.Users.First(u => u.UserName == User.Identity.Name);
               var accountNumber = user.AccountNumber;

               ViewBag.MakePaymentSmartCardNumber = "current";
               var response = new MakePaymentSmartCardNumberResponseModel();
               try
               {
                    var url = ApiConstantService.BASE_URL + type + "/payment/" + productKey + "/" + smartcardnumber;
                    var headers = new NameValueCollection();
                    headers.Add("Authorization", "Bearer " + this.UserInfo.accessToken);
                    var postData = new Dictionary<string, string>
                {
                    {"amount", amount + ""},
                    {"invoicePeriod", invoicePeriod + ""},
                    {"emailAddress", emailAddress},
                    {"accountNumber", accountNumber},
                    {"mobileNumber", mobileNumber},
                    {"paymentDescription", paymentDescription},
                    {"paymentMode", paymentMode}
                };
                    response = _request.MakeRequest<MakePaymentSmartCardNumberResponseModel>(url, postData, HttpVerb.POST, headers);
                    if (response != null)
                    {
                         var transactionEnquiriesUrl = ApiConstantService.BASE_URL + "transactions/" +
                                                       response.transactionNumber;
                         var transactionEnquiries = _request.MakeRequest<TransactionsResponseModel>(transactionEnquiriesUrl);
                         var transaction = _appDbContext.Transactions.Add(new Transaction
                         {
                              accountNumber = transactionEnquiries.accountNumber,
                              transactionDate = transactionEnquiries.transactionDate,
                              apiClientId = transactionEnquiries.apiClientId,
                              customerNumber = smartcardnumber,
                              deviceNumber = transactionEnquiries.deviceNumber,
                              packageCode = transactionEnquiries.packageCode,
                              businessType = transactionEnquiries.businessType,
                              emailAddress = transactionEnquiries.emailAddress,
                              mobileNumber = transactionEnquiries.mobileNumber,
                              amount = transactionEnquiries.amount,
                              transFees = transactionEnquiries.transFees,
                              posted = transactionEnquiries.posted,
                              auditReferenceNo = transactionEnquiries.auditReferenceNo,
                              success = transactionEnquiries.success,
                              url = transactionEnquiries.url,
                              CreatedDate = DateTime.UtcNow,
                              AgentName = user.UserName,
                              InvoicePeriod = invoicePeriod,
                         });

                         if (this._appDbContext.SaveChanges() > 0)
                         {
                              ViewBag.Id = transaction.id;
                         }
                         ShowMessage("Payment Successfull", AlertType.Success);
                         ViewBag.PaymentSucceded = true;
                    }
                    else
                    {
                         ShowMessage("Error in making payment", AlertType.Danger);
                    }

                    return View(response);
               }
               catch (Exception exception)
               {
                    Logger.WriteErrorLog(exception);
                    this.ShowMessage(exception.Message, AlertType.Danger);
                    return View(response);
               }
          }

          #region //New Payment Process Flow Changes For SmartCard Number


          [HttpPost]
          public ActionResult SelectBouquetRenewal(SelectBouquetViewModel model)
          {

               // Service Chatge Details
               int serviceCharge = 100;
               var response = serviceCharge.ToString();
               //var response = new ServiceChargeResponseModel();
               try
               {
                    var url = ApiConstantService.BASE_URL + "/servicecharge";
                    response = _request.MakeRequest<string>(url);
                    if (response.Equals(null))
                    {
                         ShowMessage("Service is Down or Unavailable and Cannot Get Service Charge", AlertType.Danger);
                         return RedirectToAction("Index", new { id = model.Type });
                    }
                    else
                    {
                         serviceCharge = Int32.Parse(response);
                    }
               }
               catch (Exception exception)
               {

                    Logger.WriteErrorLog(exception);
                    ShowMessage(exception.Message, AlertType.Danger);
                    response = serviceCharge.ToString();
                    //response = new ServiceChargeResponseModel();
               }

               // Selected Bouquet Details
               var bouquetResponse = new List<BouquetModel>();
               var selectedBouquet = new BouquetModel();
               var addOnBouquet = new BouquetModel();
               int addOnAmount = 0;

               try
               {
                    var url = ApiConstantService.BASE_URL + model.Type + "/bouquet";
                    bouquetResponse = _request.MakeRequest<List<BouquetModel>>(url);
                    if (bouquetResponse == null)
                    {
                         ShowMessage("Service is Down or Unavailable and Cannot Get Bouquet Prices", AlertType.Danger);
                         return RedirectToAction("Index", new { id = model.Type });
                    }
                    else
                    {
                         selectedBouquet = bouquetResponse.Single(e => e.code == model.SelectedBouquetCode);

                         if (!string.IsNullOrEmpty(model.AddOnBouquetCode))
                         {
                              addOnBouquet = bouquetResponse.Single(e => e.code == model.AddOnBouquetCode);
                              addOnAmount = Int32.Parse(addOnBouquet.subscription);
                         }
                    }
               }
               catch (Exception exception)
               {

                    Logger.WriteErrorLog(exception);
                    ShowMessage(exception.Message, AlertType.Danger);
                    bouquetResponse = new List<BouquetModel>();
               }


               var editModel = new PreviewMakePaymentRenewal
               {
                    Type = model.Type,
                    Amount = (Int32.Parse(selectedBouquet.subscription) * model.Period) + serviceCharge,
                    TotalAmount = (Int32.Parse(selectedBouquet.subscription) * model.Period) + serviceCharge,
                    //Amount = (1900 * model.Period) + serviceCharge,
                    //TotalAmount = (1900 * model.Period) + serviceCharge,
                    ProductKey = model.SelectedBouquetCode,
                    ServiceCharge = serviceCharge,
                    InvoicePeriod = model.Period,
                    Smartcardnumber = model.CardNumber
               };

               if (!string.IsNullOrEmpty(model.AddOnBouquetCode))
               {
                    editModel.OtherProdKey = model.AddOnBouquetCode;
                    editModel.Amount += (addOnAmount * model.Period);
                    editModel.TotalAmount += (addOnAmount * model.Period);
               }


               return View("MakePaymentRenewal", editModel);
          }

          [HttpPost]
          public ActionResult MakePaymentRenewal(PreviewMakePaymentRenewal model)
          {
               return View();
          }

          public ActionResult PreviewMakePaymentRenewal(PreviewMakePaymentRenewal model)
          {
               Session[SmartCardPayEl] = model;
               return View(model);
          }

          [HttpPost]
          public ActionResult SubmitPaymentRenewal(PreviewMakePaymentSmartCardNumberModel model)
          {
               Session.Remove(SmartCardPayEl);
               ViewBag.PaymentSucceded = false;

               var type = model.Type;
               var productKey = model.ProductKey;
               var smartcardnumber = model.Smartcardnumber;
               var amount = model.Amount;
               var invoicePeriod = model.InvoicePeriod;
               var emailAddress = model.EmailAddress;
               var mobileNumber = model.MobileNumber;
               var paymentDescription = model.PaymentDescription;
               var paymentMode = model.PaymentMode;

               AddAuditInfo(new AuditTrail
               {
                    Title = "Make Payment Customer Number",
                    Detail = $"Type: {type}, Product Key: {productKey}, Smart Card Number: {smartcardnumber},  Amount: {amount}, " +
                            $"Invoice Period: {invoicePeriod}, Email Address: {emailAddress}, Mobile Number: {mobileNumber}, Payment Mode: {paymentMode} ",
                    RefUrl = Url.Action("MakePaymentSmartCardNumber")
               });
               if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(productKey) ||
                          string.IsNullOrEmpty(smartcardnumber) ||
                          string.IsNullOrEmpty(emailAddress) ||
                          string.IsNullOrEmpty(mobileNumber)
                          || string.IsNullOrEmpty(paymentDescription) || string.IsNullOrEmpty(paymentMode))
               {
                    ShowMessage("All the fields are required.", AlertType.Warning);
                    return RedirectToAction("Index", "Home");
               }

               var user = _appDbContext.Users.First(u => u.UserName == User.Identity.Name);
               var accountNumber = user.AccountNumber;

               ViewBag.MakePaymentSmartCardNumber = "current";
               var response = new MakePaymentSmartCardNumberResponseModel();
               try
               {
                    var url = ApiConstantService.BASE_URL + type + "/payment/" + productKey + "/" + smartcardnumber;
                    var headers = new NameValueCollection();
                    headers.Add("Authorization", "Bearer " + this.UserInfo.accessToken);
                    var postData = new Dictionary<string, string>
                {
                    {"amount", amount + ""},
                    {"invoicePeriod", invoicePeriod + ""},
                    {"emailAddress", emailAddress},
                    {"accountNumber", accountNumber},
                    {"mobileNumber", mobileNumber},
                    {"paymentDescription", paymentDescription},
                    {"paymentMode", paymentMode}
                };
                    response = _request.MakeRequest<MakePaymentSmartCardNumberResponseModel>(url, postData, HttpVerb.POST, headers);
                    if (response != null)
                    {
                         var transactionEnquiriesUrl = ApiConstantService.BASE_URL + "transactions/" +
                                                       response.transactionNumber;
                         var transactionEnquiries = _request.MakeRequest<TransactionsResponseModel>(transactionEnquiriesUrl);
                         var transaction = _appDbContext.Transactions.Add(new Transaction
                         {
                              accountNumber = transactionEnquiries.accountNumber,
                              transactionDate = transactionEnquiries.transactionDate,
                              apiClientId = transactionEnquiries.apiClientId,
                              customerNumber = smartcardnumber,
                              deviceNumber = transactionEnquiries.deviceNumber,
                              packageCode = transactionEnquiries.packageCode,
                              businessType = transactionEnquiries.businessType,
                              emailAddress = transactionEnquiries.emailAddress,
                              mobileNumber = transactionEnquiries.mobileNumber,
                              amount = transactionEnquiries.amount,
                              transFees = transactionEnquiries.transFees,
                              posted = transactionEnquiries.posted,
                              auditReferenceNo = transactionEnquiries.auditReferenceNo,
                              success = transactionEnquiries.success,
                              url = transactionEnquiries.url,
                              CreatedDate = DateTime.UtcNow,
                              AgentName = user.UserName,
                              InvoicePeriod = invoicePeriod,
                         });

                         if (this._appDbContext.SaveChanges() > 0)
                         {
                              ViewBag.Id = transaction.id;
                         }
                         ShowMessage("Payment Successfull", AlertType.Success);
                         ViewBag.PaymentSucceded = true;
                    }
                    else
                    {
                         ShowMessage("Error in making payment", AlertType.Danger);
                    }

                    return View(response);
               }
               catch (Exception exception)
               {
                    Logger.WriteErrorLog(exception);
                    this.ShowMessage(exception.Message, AlertType.Danger);
                    return View(response);
               }
          }

          // Make Payment for Upgrade or Downgrade

          [HttpPost]
          public ActionResult SelectBouquetUpgrade(SelectBouquetViewModel model)
          {

               // Service Chatge Details
               int serviceCharge = 100;
               var response = serviceCharge.ToString();
               try
               {
                    var url = ApiConstantService.BASE_URL + "/servicecharge";
                    response = _request.MakeRequest<string>(url);
                    if (response.Equals(null))
                    {
                         ShowMessage("Service is Down or Unavailable and Cannot Get Service Charge", AlertType.Danger);
                         return RedirectToAction("Index", new { id = model.Type });
                    }
                    else
                    {
                         serviceCharge = Int32.Parse(response);
                    }
               }
               catch (Exception exception)
               {

                    Logger.WriteErrorLog(exception);
                    ShowMessage(exception.Message, AlertType.Danger);
                    //response = new ServiceChargeResponseModel();
               }

               // Selected Bouquet Details
               var bouquetResponse = new List<BouquetModel>();
               var selectedBouquet = new BouquetModel();
               var addOnBouquet = new BouquetModel();
               int addOnAmount = 0;

               try
               {
                    var url = ApiConstantService.BASE_URL + model.Type + "/bouquet";
                    bouquetResponse = _request.MakeRequest<List<BouquetModel>>(url);
                    if (bouquetResponse == null)
                    {
                         ShowMessage("Service is Down or Unavailable and Cannot Get Bouquet Prices", AlertType.Danger);
                         return RedirectToAction("Index", new { id = model.Type });
                    }
                    else
                    {
                         selectedBouquet = bouquetResponse.Single(e => e.code == model.SelectedBouquetCode);

                         if (!string.IsNullOrEmpty(model.AddOnBouquetCode))
                         {
                              addOnBouquet = bouquetResponse.Single(e => e.code == model.AddOnBouquetCode);
                              addOnAmount = Int32.Parse(addOnBouquet.subscription);
                         }
                    }
               }
               catch (Exception exception)
               {

                    Logger.WriteErrorLog(exception);
                    ShowMessage(exception.Message, AlertType.Danger);
                    bouquetResponse = new List<BouquetModel>();
               }


               var editModel = new PreviewMakePaymentUpgrade
               {
                    Type = model.Type,
                    Amount = (Int32.Parse(selectedBouquet.subscription) * model.Period) + serviceCharge,
                    TotalAmount = (Int32.Parse(selectedBouquet.subscription) * model.Period) + serviceCharge,
                    //Amount = (1900 * model.Period) + serviceCharge,
                    //TotalAmount = (1900 * model.Period) + serviceCharge,
                    ProductKey = model.SelectedBouquetCode,
                    ServiceCharge = serviceCharge,
                    InvoicePeriod = model.Period,
                    Smartcardnumber = model.CardNumber
               };

               if (!string.IsNullOrEmpty(model.AddOnBouquetCode))
               {
                    editModel.OtherProdKey = model.AddOnBouquetCode;
                    editModel.Amount += (addOnAmount * model.Period);
                    editModel.TotalAmount += (addOnAmount * model.Period);
               }


               return View("MakePaymentUpgrade", editModel);
          }



          [HttpPost]
          public ActionResult MakePaymentUpgrade(PreviewMakePaymentUpgrade model)
          {
               return View();
          }

          public ActionResult PreviewMakePaymentUpgrade(PreviewMakePaymentUpgrade model)
          {
               Session[SmartCardPayEl] = model;
               return View(model);
          }

          [HttpPost]
          public ActionResult SubmitPaymentUpgrade(PreviewMakePaymentSmartCardNumberModel model)
          {
               Session.Remove(SmartCardPayEl);
               ViewBag.PaymentSucceded = false;

               var type = model.Type;
               var productKey = model.ProductKey;
               var smartcardnumber = model.Smartcardnumber;
               var amount = model.Amount;
               var invoicePeriod = model.InvoicePeriod;
               var emailAddress = model.EmailAddress;
               var mobileNumber = model.MobileNumber;
               var paymentDescription = model.PaymentDescription;
               var paymentMode = model.PaymentMode;

               AddAuditInfo(new AuditTrail
               {
                    Title = "Make Payment Customer Number",
                    Detail = $"Type: {type}, Product Key: {productKey}, Smart Card Number: {smartcardnumber},  Amount: {amount}, " +
                            $"Invoice Period: {invoicePeriod}, Email Address: {emailAddress}, Mobile Number: {mobileNumber}, Payment Mode: {paymentMode} ",
                    RefUrl = Url.Action("MakePaymentSmartCardNumber")
               });
               if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(productKey) ||
                          string.IsNullOrEmpty(smartcardnumber) ||
                          string.IsNullOrEmpty(emailAddress) ||
                          string.IsNullOrEmpty(mobileNumber)
                          || string.IsNullOrEmpty(paymentDescription) || string.IsNullOrEmpty(paymentMode))
               {
                    ShowMessage("All the fields are required.", AlertType.Warning);
                    return RedirectToAction("Index", "Home");
               }

               var user = _appDbContext.Users.First(u => u.UserName == User.Identity.Name);
               var accountNumber = user.AccountNumber;

               ViewBag.MakePaymentSmartCardNumber = "current";
               var response = new MakePaymentSmartCardNumberResponseModel();
               try
               {
                    var url = ApiConstantService.BASE_URL + type + "/payment/" + productKey + "/" + smartcardnumber;
                    var headers = new NameValueCollection();
                    headers.Add("Authorization", "Bearer " + this.UserInfo.accessToken);
                    var postData = new Dictionary<string, string>
                {
                    {"amount", amount + ""},
                    {"invoicePeriod", invoicePeriod + ""},
                    {"emailAddress", emailAddress},
                    {"accountNumber", accountNumber},
                    {"mobileNumber", mobileNumber},
                    {"paymentDescription", paymentDescription},
                    {"paymentMode", paymentMode}
                };
                    response = _request.MakeRequest<MakePaymentSmartCardNumberResponseModel>(url, postData, HttpVerb.POST, headers);
                    if (response != null)
                    {
                         var transactionEnquiriesUrl = ApiConstantService.BASE_URL + "transactions/" +
                                                       response.transactionNumber;
                         var transactionEnquiries = _request.MakeRequest<TransactionsResponseModel>(transactionEnquiriesUrl);
                         var transaction = _appDbContext.Transactions.Add(new Transaction
                         {
                              accountNumber = transactionEnquiries.accountNumber,
                              transactionDate = transactionEnquiries.transactionDate,
                              apiClientId = transactionEnquiries.apiClientId,
                              customerNumber = smartcardnumber,
                              deviceNumber = transactionEnquiries.deviceNumber,
                              packageCode = transactionEnquiries.packageCode,
                              businessType = transactionEnquiries.businessType,
                              emailAddress = transactionEnquiries.emailAddress,
                              mobileNumber = transactionEnquiries.mobileNumber,
                              amount = transactionEnquiries.amount,
                              transFees = transactionEnquiries.transFees,
                              posted = transactionEnquiries.posted,
                              auditReferenceNo = transactionEnquiries.auditReferenceNo,
                              success = transactionEnquiries.success,
                              url = transactionEnquiries.url,
                              CreatedDate = DateTime.UtcNow,
                              AgentName = user.UserName,
                              InvoicePeriod = invoicePeriod,
                         });

                         if (this._appDbContext.SaveChanges() > 0)
                         {
                              ViewBag.Id = transaction.id;
                         }
                         ShowMessage("Payment Successfull", AlertType.Success);
                         ViewBag.PaymentSucceded = true;
                    }
                    else
                    {
                         ShowMessage("Error in making payment", AlertType.Danger);
                    }

                    return View(response);
               }
               catch (Exception exception)
               {
                    Logger.WriteErrorLog(exception);
                    this.ShowMessage(exception.Message, AlertType.Danger);
                    return View(response);
               }
          }




          public ActionResult PreviewMakePaymentBoxOffice()
          {
               return View();
          }

          [HttpPost]
          public ActionResult MakePaymentBoxOffice()
          {
               return View();
          }

          #endregion


          public ActionResult Receipt(int id)
          {
               AddAuditInfo(new AuditTrail
               {
                    Title = "Print Receipt",
                    Detail = "Id: " + id
               });

               var transaction = _appDbContext.Transactions.Find(id);
               var user = _appDbContext.Users.First(u => u.UserName == User.Identity.Name);

               if (transaction == null)
               {
                    ShowMessage("Transaction was not found.", AlertType.Warning);
                    return RedirectToAction("Index", "Home");
               }
               transaction.PrintedReceipt = true;
               _appDbContext.SaveChanges();
               return View(new RepeiptViewModel { Transaction = transaction, User = user });
          }
          protected override void Dispose(bool disposing)
          {
               if (disposing)
               {
                    this._appDbContext.Dispose();
                    this._appDbContext = null;
               }
               base.Dispose(disposing);
          }


     }
}