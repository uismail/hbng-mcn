﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MultichoiceCollection.Presentation.Models
{
    public class PreviewMakePaymentSmartCardNumberModel
    {
        public string Type { get; set; }
        public string ProductKey { get; set; }
        public string Smartcardnumber { get; set; }
        public decimal Amount { get; set; }
        public int InvoicePeriod { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNumber { get; set; }
        public string PaymentDescription { get; set; }
        public string PaymentMode { get; set; }
    }
}