﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultichoiceCollection.Presentation.Models
{
     public class SelectBouquetViewModel
     {
          public List<BouquetModel> Bouquets { get; set; }
          public int Period { get; set; }
          public string Type { get; set; }
          public string PayEl { get; set; }
          public string CardNumber { get; set; }
          public string SelectedBouquetCode { get; set; }
          public string AddOnBouquetCode { get; set; }
     }
}
