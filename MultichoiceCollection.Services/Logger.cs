﻿using System;
using System.Configuration;
using System.IO;

namespace MultichoiceCollection.Services
{
    public class Logger
    {
        public static void WriteErrorLog(Exception ex, string extras = "", bool addTrace = true)
        {
            try
            {
                var errorLogFilename = "ErrorLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                var path = ConfigurationManager.AppSettings["logDir"];
                if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                if (path.EndsWith("/") || path.EndsWith("\\")) path = path.Substring(0, path.Length - 1);
                path = $"{path}/{errorLogFilename}";

                if (File.Exists(path))
                {
                    using (var stwriter = new StreamWriter(path, true))
                    {
                        stwriter.WriteLine("-------------------Error Log Start-----------as on " + DateTime.Now.ToString("hh:mm tt"));
                        stwriter.WriteLine("Extras: " + extras);
                        stwriter.WriteLine("Message: " + ex);
                        if (addTrace)
                            stwriter.WriteLine("Stack Trace: ", ex.StackTrace);
                        stwriter.WriteLine("-------------------End----------------------------");
                    }
                }
                else
                {
                    var stwriter = File.CreateText(path);

                    stwriter.WriteLine("-------------------Error Log Start-----------as on " + DateTime.Now.ToString("hh:mm tt"));
                    stwriter.WriteLine("Extras: " + extras);
                    stwriter.WriteLine("Message: " + ex);
                    if (addTrace)
                        stwriter.WriteLine("Stack Trace: ", ex.StackTrace);
                    stwriter.WriteLine("-------------------End----------------------------");

                    stwriter.Close();
                }
            }
            catch (Exception execption)
            {
                throw new Exception(execption.Message + ". Logging - " + ex.Message);
            }

        }
    }
}
